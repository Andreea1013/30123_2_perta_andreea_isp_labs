package ex5;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class Simulator extends JFrame {
    JComboBox controlerComboBox;
    JTextArea segmentTextArea;
    JTextField trainNameTextField;
    JTextField destinationTextField;
    JTextField segmentTextField;

    public Simulator() {
        setTitle("Train Stations");
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        initComponents();
        setSize(800, 700);
        setVisible(true);
    }

    public void initComponents() {
        setLayout(null);

        //===========================================================
        Controler c1 = new Controler("Cluj-Napoca");

        Segment s1 = new Segment(1);
        Segment s2 = new Segment(2);
        Segment s3 = new Segment(3);

        c1.addControlledSegment(s1);
        c1.addControlledSegment(s2);
        c1.addControlledSegment(s3);

        //build station Bucuresti
        Controler c2 = new Controler("Bucuresti");

        Segment s4 = new Segment(4);
        Segment s5 = new Segment(5);
        Segment s6 = new Segment(6);

        c2.addControlledSegment(s4);
        c2.addControlledSegment(s5);
        c2.addControlledSegment(s6);

        //build one more controller
        Controler c3 = new Controler("Sibiu");
        c3.addControlledSegment(s4);
        c3.addControlledSegment(s5);

        //connect the 2 controllers

        c1.setNeighbourController(c2);
        c2.setNeighbourController(c1);
        c2.setNeighbourController(c3);

        //build Trains
        Train t1 = new Train("Bucuresti", "IC-001");
        s1.arriveTrain(t1);       //arriveTrain seteaza trenul segmentului s

        Train t2 = new Train("Cluj-Napoca", "R-002");
        s5.arriveTrain(t2);

        Train t3 = new Train("Cluj-Napoca", "IC-666");
        s6.arriveTrain(t3);

        //============================================================


        JLabel stationJlabel = new JLabel();
        stationJlabel.setText("Select a station: ");
        stationJlabel.setBounds(50, 70, 100, 20);

        controlerComboBox = new JComboBox();
        controlerComboBox.setBounds(50, 100, 200, 20);
        controlerComboBox.addItem(c1);
        controlerComboBox.addItem(c2);


        controlerComboBox.addActionListener(new comboBoxCallback());

        segmentTextArea = new JTextArea();
        segmentTextArea.setBounds(300, 100, 300, 200);

        JLabel segmentInfoJlabel = new JLabel();
        segmentInfoJlabel.setText("Information about segments for the selected station:");
        segmentInfoJlabel.setBounds(300, 70, 300, 20);


        JLabel nameTrain = new JLabel();
        nameTrain.setText("Enter the train: ");
        nameTrain.setBounds(50, 500, 300, 30);

        JLabel destinationStationJlabel = new JLabel();
        destinationStationJlabel.setText("Enter train destination: ");
        destinationStationJlabel.setBounds(50, 525, 300, 30);

        JLabel segmentDestinationJlabel = new JLabel();
        segmentDestinationJlabel.setText("Enter the segment where the train to be placed: ");
        segmentDestinationJlabel.setBounds(50, 550, 300, 30);

        JButton addTrainButton = new JButton();
        addTrainButton.setText("Add Train");
        addTrainButton.setBounds(600, 530, 100, 50);
        addTrainButton.addActionListener(new addTrainButtonCallback());

        trainNameTextField = new JTextField();
        trainNameTextField.setBounds(350, 510, 200, 20);

        destinationTextField = new JTextField();
        destinationTextField.setBounds(350, 533, 200, 20);

        segmentTextField = new JTextField();
        segmentTextField.setBounds(350, 555, 200, 20);


        add(controlerComboBox);
        add(segmentTextArea);
        add(stationJlabel);
        add(segmentInfoJlabel);
        add(destinationStationJlabel);
        add(segmentDestinationJlabel);
        add(addTrainButton);
        add(destinationTextField);
        add(segmentTextField);
        add(nameTrain);
        add(trainNameTextField);

    }

    class comboBoxCallback implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            segmentTextArea.setText(null);

            Controler selectedControler = (Controler) controlerComboBox.getSelectedItem();


            for (Segment s : selectedControler.list) {
                if (s.hasTrain()) {
                    segmentTextArea.append("Segment " + s.id + " with train " + s.getTrain().getName() + "\n");
                } else {
                    segmentTextArea.append("Segment " + s.id + " free segment \n");
                }
            }
        }
    }

    class addTrainButtonCallback implements ActionListener {
        public void actionPerformed(ActionEvent e) {
//            trainNameTextField.getText();
//            destinationTextField.getText();
//            segmentTextField.getText();
            addTrainInStationByDestinationAndSegment(trainNameTextField.getText(), destinationTextField.getText(), Integer.parseInt(segmentTextField.getText()));
        }
    }

    public void addTrainInStationByDestinationAndSegment(String t, String destination, int id) {
        Train userAddedTrain = new Train(destination, t);
        JOptionPane.showMessageDialog(null, "Train Added");
    }

    public static void main(String[] args) {

        new Simulator();
    }
}
