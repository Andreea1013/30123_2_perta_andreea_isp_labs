package ex2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class IncrementButton extends JFrame {
    private JButton button1;
    private JTextField textField1;
    private JPanel Panel;
    int counter = 0;

    public IncrementButton() {

        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                counter++;
                textField1.setText(counter + " ");
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("IncrementButton");
        frame.setContentPane(new IncrementButton().Panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}




