package ex3;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class ex3 {
    private JButton button1;
    private JTextArea textArea1;
    private JPanel Panel;
    private JTextField textField1;

    public ex3() {
        textField1.setText("src/main/resources/ex3File");
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    textArea1.setText(Files.readString(Paths.get(textField1.getText())));
                } catch (IOException ioException) {
                    JOptionPane.showMessageDialog(null,"File not found");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("ex3");
        frame.setContentPane(new ex3().Panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
