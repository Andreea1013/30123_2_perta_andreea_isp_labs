package ex4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static java.lang.Integer.parseInt;

public class ex4 {
    private JPanel panel1;
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JButton button4;
    private JButton button5;
    private JButton button6;
    private JButton button7;
    private JButton button8;
    private JButton button9;
    int numberOfActionPerformed = 0;


    public ex4() {

        ActionListener listener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Object o = new Object();
                o = e.getSource();
                //X-first player
                //0-second player
                if (numberOfActionPerformed % 2 == 0) {
                    ((JButton) o).setText("X");
                    numberOfActionPerformed++;
                } else if (numberOfActionPerformed % 2 == 1) {
                    ((JButton) o).setText("0");
                    numberOfActionPerformed++;
                }

                if (findWinner()) {
                    button1.setEnabled(false);
                    button2.setEnabled(false);
                    button3.setEnabled(false);
                    button4.setEnabled(false);
                    button5.setEnabled(false);
                    button6.setEnabled(false);
                    button7.setEnabled(false);
                    button8.setEnabled(false);
                    button9.setEnabled(false);
                }
            }

        };
        button1.addActionListener(listener);
        button2.addActionListener(listener);
        button3.addActionListener(listener);
        button4.addActionListener(listener);
        button5.addActionListener(listener);
        button6.addActionListener(listener);
        button7.addActionListener(listener);
        button8.addActionListener(listener);
        button9.addActionListener(listener);
    }

    public boolean findWinner() {
        if ((button1.getText() == "X" && button2.getText() == "X" && button3.getText() == "X") ||
                (button4.getText() == "X" && button5.getText() == "X" && button6.getText() == "X") ||
                (button7.getText() == "X" && button8.getText() == "X" && button9.getText() == "X") ||
                (button1.getText() == "X" && button4.getText() == "X" && button7.getText() == "X") ||
                (button2.getText() == "X" && button5.getText() == "X" && button8.getText() == "X") ||
                (button3.getText() == "X" && button6.getText() == "X" && button9.getText() == "X") ||
                (button1.getText() == "X" && button5.getText() == "X" && button9.getText() == "X") ||
                (button3.getText() == "X" && button5.getText() == "X" && button7.getText() == "X")
        ) {
            JOptionPane.showMessageDialog(null, "First Player wins!");
            return true;
        } else if ((button1.getText() == "0" && button2.getText() == "0" && button3.getText() == "0") ||
                (button4.getText() == "0" && button5.getText() == "0" && button6.getText() == "0") ||
                (button7.getText() == "0" && button8.getText() == "0" && button9.getText() == "0") ||
                (button1.getText() == "0" && button4.getText() == "0" && button7.getText() == "0") ||
                (button2.getText() == "0" && button5.getText() == "0" && button8.getText() == "0") ||
                (button3.getText() == "0" && button6.getText() == "0" && button9.getText() == "0") ||
                (button1.getText() == "0" && button5.getText() == "0" && button9.getText() == "0") ||
                (button3.getText() == "0" && button5.getText() == "0" && button7.getText() == "0")
        ) {
            JOptionPane.showMessageDialog(null, "Second Player wins!");
            return true;
        } else if (numberOfActionPerformed > 8) {
            JOptionPane.showMessageDialog(null, "Draw!");
            return true;

        }
        return false;
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("ex4");
        frame.setContentPane(new ex4().panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}

