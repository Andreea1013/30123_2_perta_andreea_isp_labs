package ex4;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestMyPiont {

    @Test
    void TestConstructor1() {
        MyPoint p1 = new MyPoint();
        assertEquals(0, p1.getX());
        assertEquals(0, p1.getY());
    }

    @Test
    void TestConstructor2() {
        MyPoint p1 = new MyPoint(5,6);
        assertEquals(5, p1.getX());
        assertEquals(6, p1.getY());
    }

    @Test
    void TesttoString() {
        MyPoint p1 = new MyPoint(5,6);
        assertEquals("(5,6)", p1.toString());
    }

    @Test
    void Testdistance() {
        MyPoint p1 = new MyPoint(5,6);
        assertEquals( 5.656854249492381, p1.distance(1,2));
    }

    @Test
    void Testdistance1() {
        MyPoint p1 = new MyPoint(5,6);
        MyPoint p2= new MyPoint(1,2);
        assertEquals( 5.656854249492381, p1.distance1(p2)) ;
    }

    @Test
    void TestsetXsetYsetXY() {
        MyPoint p1 = new MyPoint(5,6);
        assertEquals(5, p1.getX());
        assertEquals(6, p1.getY());
        p1.setX(3);
        p1.setY(8);
        assertEquals(3, p1.getX());
        assertEquals(8, p1.getY());
        MyPoint p2 = new MyPoint();
        assertEquals(0, p2.getX());
        assertEquals(0, p2.getY());
        p2.setXY(4,7);
        assertEquals(4, p2.getX());
        assertEquals(7, p2.getY());

    }

}
