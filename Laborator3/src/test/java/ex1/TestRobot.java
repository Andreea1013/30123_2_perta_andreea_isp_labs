package ex1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestRobot {

    @Test
    void TestConstructor() {
        Robot robot = new Robot();
        assertEquals(1, robot.getX());
    }

    @Test
    void TestChange() {
        Robot robot = new Robot();
        assertEquals(1, robot.getX());
        robot.change(2);
        assertEquals(3, robot.getX());
    }

    @Test
    void TesttoString(){
        Robot robot = new Robot();
        assertEquals("1", robot.toString());
    }
}
