package ex5;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestFlower {

    @Test
    void TestConstructor(){
        Flower f= new Flower(5);
        assertEquals(5,f.getPetal());
    }

    @Test
    void TestgetNr(){
        Flower f1= new Flower(5);
        assertEquals(5,f1.getPetal());
        Flower f2= new Flower(3);
        assertEquals(3,f2.getPetal());
        Flower f3= new Flower(7);
        assertEquals(7,f3.getPetal());
        assertEquals(3,f3.getNr());

    }

}
