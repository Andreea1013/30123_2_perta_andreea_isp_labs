package ex5;

public class Flower {
    private int petal;
    private static int nr=0;

    Flower(int p) {
        petal = p;
        nr++;
        System.out.println("New flower has been created!");
    }

    public int getPetal(){
        return this.petal;
    }

    public static int getNr(){
        return nr;
    }


}