package ex4;

public class MyPoint {

    public int x;
    public int y;
    public MyPoint(){
        this.x=0;
        this.y=0;
    }
    public MyPoint(int x0, int y0){
        this.x=x0;
        this.y=y0;
    }
    public int getX(){
        return x;
    }
    public void setX(int x0){
        this.x=x0;
    }
    public int getY(){
        return y;
    }
    public void setY(int y0){
        this.y=y0;
    }
    public void setXY(int x0, int y0){
        this.x=x0;
        this.y=y0;
    }
    public String toString(){
        return "("+this.x+","+this.y+")";
    }
    public double distance(int x,int y){
        return Math.sqrt(Math.pow((this.x-x),2)+Math.pow((this.y-y),2));
    }
    public double distance1(MyPoint another){
        return Math.sqrt(Math.pow((this.x-another.x),2)+Math.pow((this.y-another.y),2));
    }

}
