package ex5;

import ex1.Circle;

public class Cylinder extends Circle {
    private double height = 1.0;

    public Cylinder() {
    }

    public Cylinder(double r) {
        super(r);
    }

    public Cylinder(double r, double height) {
        super(r);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return super.getArea() * height;
    }

    @Override
    public double getArea() {
        return 2 * super.getArea() + 2 * Math.PI * super.getRadius() * height;
    }


}

