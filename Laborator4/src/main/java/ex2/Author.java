package ex2;

public class Author {
    private String name;
    private String email;
    private char gender;

    public Author(String name1, String email1, char g) {
        this.name = name1;
        this.email = email1;
        if (g == 'm' || g == 'f') {
            this.gender = g;
        }
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email2) {
        this.email = email2;
    }

    public char getGender() {
        return gender;
    }

    public String toString() {
        return this.name + " (" + this.gender + ") at " + this.email;
    }
}
