package ex1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCircle {
    @Test
    void TestConstructor1() {
        Circle c1 = new Circle();
        assertEquals(1.0, c1.getRadius());
        assertEquals("red", c1.getColor());
    }

    @Test
    void TestConstructor2() {
        Circle c2 = new Circle(5.0);
        assertEquals(5.0, c2.getRadius());
        assertEquals("red", c2.getColor());
    }

    @Test
    void TestgetArea() {
        Circle c2 = new Circle(2.0);
        assertEquals(12.566370614359172, c2.getArea());
    }
}
