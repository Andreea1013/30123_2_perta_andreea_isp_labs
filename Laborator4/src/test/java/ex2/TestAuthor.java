package ex2;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestAuthor {
    @Test
    void TestConstructor() {
        Author a = new Author("Elena", "elena123@yahoo.com", 'f');
        assertEquals("Elena", a.getName());
        assertEquals("elena123@yahoo.com", a.getEmail());
        assertEquals('f', a.getGender());
    }

    @Test
    void TestsetEmail() {
        Author a = new Author("Elena", "elena123@yahoo.com", 'f');
        a.setEmail("elena@gmail.com");
        assertEquals("Elena", a.getName());
        assertEquals("elena@gmail.com", a.getEmail());
        assertEquals('f', a.getGender());
    }


}
