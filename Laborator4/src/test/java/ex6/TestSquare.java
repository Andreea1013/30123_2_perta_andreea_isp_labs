package ex6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestSquare {
    @Test
    void TestConstructor1() {
        Square s1 = new Square();
        assertEquals(1.0, s1.getSide());
    }

    @Test
    void TestConstructor2() {
        Square s2 = new Square(4.0);
        assertEquals(4.0, s2.getSide());
    }

    @Test
    void TestConstructor3() {
        Square s3 = new Square(8.0, "blue", false);
        assertEquals("blue", s3.getColor());
        assertEquals(false, s3.isFilled());
        assertEquals(8.0, s3.getSide());
    }

}
