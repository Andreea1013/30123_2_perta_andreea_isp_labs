package ex6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestShape {
    @Test
    void TestConstructor1() {
        Shape s1 = new Shape();
        assertEquals("green", s1.getColor());
        assertEquals(true, s1.isFilled());
    }

    @Test
    void TestConstructor2() {
        Shape s2 = new Shape("pink", false);
        assertEquals("pink", s2.getColor());
        assertEquals(false, s2.isFilled());
    }

}
