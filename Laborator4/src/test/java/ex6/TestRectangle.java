package ex6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestRectangle {
    @Test
    void TestConstructor1() {
        Rectangle r1 = new Rectangle();
        assertEquals(1.0, r1.getWidth());
        assertEquals(1.0, r1.getLength());
    }

    @Test
    void TestConstructor2() {
        Rectangle r2 = new Rectangle(3.0, 4.0);
        assertEquals(3.0, r2.getWidth());
        assertEquals(4.0, r2.getLength());
    }

    @Test
    void TestConstructor3() {
        Rectangle r3 = new Rectangle(5.0, 9.0, "white", true);
        assertEquals("white", r3.getColor());
        assertEquals(true, r3.isFilled());
        assertEquals(5.0, r3.getWidth());
        assertEquals(9.0, r3.getLength());
    }

    @Test
    void TestgetArea() {
        Rectangle r2 = new Rectangle(3.0, 4.0);
        assertEquals(12.0, r2.getArea());
    }

    @Test
    void TestgetPerimeter() {
        Rectangle r2 = new Rectangle(3.0, 4.0);
        assertEquals(14.0, r2.getPerimeter());
    }

}
