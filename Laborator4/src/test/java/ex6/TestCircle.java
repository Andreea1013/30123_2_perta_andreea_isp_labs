package ex6;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCircle {
    @Test
    void TestConstructor1() {
        Circle c1 = new Circle();
        assertEquals(1.0, c1.getRadius());
    }

    @Test
    void TestConstructor2() {
        Circle c2 = new Circle(5.0);
        assertEquals(5.0, c2.getRadius());
    }

    @Test
    void TestConstructor3() {
        Circle c3 = new Circle("white", true, 3.0);
        assertEquals("white", c3.getColor());
        assertEquals(true, c3.isFilled());
        assertEquals(3.0, c3.getRadius());
    }

    @Test
    void TestgetArea() {
        Circle c2 = new Circle(5.0);
        assertEquals(78.53981633974483, c2.getArea());
    }

    @Test
    void TestgetPerimeter() {
        Circle c2 = new Circle(5.0);
        assertEquals(31.41592653589793, c2.getPerimeter());
    }
}
