package ex5;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestCylinder {
    @Test
    void TestConstructor1() {
        Cylinder c1 = new Cylinder();
        assertEquals(1.0, c1.getRadius());
        assertEquals("red", c1.getColor());
    }

    @Test
    void TestConstructor2() {
        Cylinder c2 = new Cylinder(5.0);
        assertEquals(5.0, c2.getRadius());
        assertEquals("red", c2.getColor());
    }

    @Test
    void TestConstructor3() {
        Cylinder c3 = new Cylinder(3.0, 7.0);
        assertEquals(3.0, c3.getRadius());
        assertEquals("red", c3.getColor());
        assertEquals(7.0, c3.getHeight());
    }

    @Test
    void TestgetVolume() {
        Cylinder c3 = new Cylinder(3.0, 7.0);
        assertEquals(197.92033717615698, c3.getVolume());
    }

    @Test
    void TestgetArea() {
        Cylinder c3 = new Cylinder(3.0, 7.0);
        assertEquals(188.4955592153876, c3.getArea());
    }

}
