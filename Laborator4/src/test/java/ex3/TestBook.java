package ex3;

import ex2.Author;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestBook {
    @Test
    void TestConstructor1() {
        Author a1 = new Author("Elena", "elena123@yahoo.com", 'f');
        Book b1 = new Book("Book1", a1, 50.00);
        assertEquals("Book1", b1.getName());
        assertEquals(a1, b1.getAuthor());
        assertEquals(50.00, b1.getPrice());
    }

    @Test
    void TestConstructor2() {
        Author a2 = new Author("Dan", "dan123@yahoo.com", 'm');
        Book b2 = new Book("Book2", a2, 60.00, 10);
        assertEquals("Book2", b2.getName());
        assertEquals(a2, b2.getAuthor());
        assertEquals(60.00, b2.getPrice());
        assertEquals(10, b2.getQtyInStock());
    }
}

