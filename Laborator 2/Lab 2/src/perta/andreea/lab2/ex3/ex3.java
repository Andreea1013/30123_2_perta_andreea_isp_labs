package perta.andreea.lab2.ex3;
import java.lang.Math;
import java.util.Scanner;
public class ex3 {

    static boolean verifprim(int n) {

        if (n < 2) {
            return false;
        } else if (n == 2) {
            return true;
        } else if (n % 2 == 0) {
            return false;
        } else {
            for (int i = 3; i <= Math.sqrt(n); i = i + 2) {
                if (n % i == 0) {
                    return false;
                }
            }
            return true;
        }
    }

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Introduceti un numar de la tastatura: ");
        int A = in.nextInt();
        System.out.println("Introduceti un numar de la tastatura: ");
        int B = in.nextInt();
        int k = 0, aux;
        if (A > B) {
            aux = A;
            A = B;
            B = aux;
        }
        for (int i = A; i <= B; i++) {
            if (verifprim(i)) {
                System.out.print(i + " ");
                k++;
            }

        }
        if(k==0){
            System.out.println("Nu sunt numere prime");
        } else {
                System.out.println( '\n'+ "Sunt " + k + " numere prime");
        }
    }

}
