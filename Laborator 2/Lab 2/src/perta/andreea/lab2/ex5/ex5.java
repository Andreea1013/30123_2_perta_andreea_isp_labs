package perta.andreea.lab2.ex5;
import java.util.*;

public class ex5 {
    public static void main(String[] args) {

        Random r = new Random();
        int[] a = new int[10];
        for (int i = 0; i < 10; i++) {
            a[i] = r.nextInt(100);
        }

        for (int i = 0; i < 10; i++) {
            System.out.print("a[" + i + "]=" + a[i] + " ");
        }
        Bubble(a);
        System.out.println('\n'+"Vectorul sortat: ");
        for (int i = 0; i < 10; i++) {
            System.out.print("a[" + i + "]=" + a[i] + " ");
        }
    }


    private static void Bubble(int[] v) {
        int n = 10;
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (v[j] > v[j + 1]) {
                    int aux = v[j];
                    v[j] = v[j + 1];
                    v[j + 1] = aux;
                }
    }
}