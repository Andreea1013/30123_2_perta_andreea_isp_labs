package perta.andreea.lab2.ex2;
import java.util.Scanner;
public class ex2 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Introduceti un numar de la tastatura: ");
        int number = in.nextInt();
        ifMetod(number);
        switchMethod(number);
    }

    private static void ifMetod(int number){
        if (number==1)

        {System.out.println("One");}
        else if (2 == number)

        { System.out.println("TWO");}
        else if(number==3)

        {System.out.println("THREE");}
        else  if(number==4)


        { System.out.println("FOUR");}
        else if(number==5)
        {System.out.println("FIVE");}
        else if(number==6)
        {
            System.out.println("SIX");}
        else if (number==7)
        {
            System.out.println("SEVEN");}
        else if(number==8)
        {
            System.out.println("EIGHT");
        }
        else if(number==9)
        {System.out.println("NINE");}
        else
            System.out.println("OTHER");

    }


    private static void switchMethod(int number){
        switch (number) {
            case 1:
                System.out.println("ONE");
                break;
            case 2:
                System.out.println("TWO");
                break;
            case 3:
                System.out.println("THREE");
                break;
            case 4:
                System.out.println("FOUR");
                break;
            case 5:
                System.out.println("FIVE");
                break;
            case 6:
                System.out.println("SIX");
                break;
            case 7:
                System.out.println("SEVEN");
                break;
            case 8:
                System.out.println("EIGHT");
                break;
            case 9:
                System.out.println("NINE");
                break;
            default:
                System.out.println("OTHER");
        }

    }

}
