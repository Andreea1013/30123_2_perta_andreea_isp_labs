package perta.andreea.lab2.ex6;

import java.util.Scanner;

public class ex6 {

    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Introduceti un nunmar:");
        int n = in.nextInt();
        System.out.println("Factorial recursiv:"+ " " +Recursiv(n));
        System.out.println("Factorial nerecursiv:"+ " " + Nerecursiv(n));

    }

    private static int Recursiv(int n) {
        if (n==0){
            return 1;
        }
        else return n*Recursiv(n-1);

    }

    private static int Nerecursiv(int n) {
        if (n==0){
            return 1;
        }
        int p=1;
        for (int i=1;i<=n;i++){
            p=p*i;
        }
        return p;
    }

}
