package perta.andreea.lab2.ex4;

import java.util.Scanner;

public class ex4 {
    public static void main(String[] args){
        Scanner in = new Scanner(System.in);
        System.out.println("Numarul de elemente: ");
        int n = in.nextInt();
        int [] v = new int[n];

        System.out.println("Elementele:");
        for (int i=0;i<n;i++){
            System.out.println("v[ "+i+"]=");
            v[i]=in.nextInt();
        }

        int max=v[0];
        for (int i=1;i<n;i++){
            if (v[i]>max){
                max=v[i];
            }
        }
        System.out.println("numarul maxim este:"+max);

    }

}
