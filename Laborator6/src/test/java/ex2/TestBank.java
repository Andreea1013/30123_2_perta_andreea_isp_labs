package ex2;

import ex1.BankAccount;
import ex2.Bank;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestBank {
    @Test
    void TestaddAccount() {
        Bank bank = new Bank();
        BankAccount b = new BankAccount("Ana", 1625.1);
        bank.addAccount("Ana", 1625.1);
        assertEquals(b, bank.getAccount("Ana"));
    }
}
