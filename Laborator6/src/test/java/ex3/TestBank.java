package ex3;

import ex1.BankAccount;
import ex3.Bank;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestBank {
    @Test
    void TestaddAccount() {
        Bank bank = new Bank();
        BankAccount b = new BankAccount("Alex", 444.4);
        bank.addAccount("Alex", 444.4);
        assertEquals(b, bank.getAccount("Alex"));

    }
}
