package ex3;

public class Main {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("Alin", 655.4);
        bank.addAccount("Maria", 166.5);
        bank.addAccount("Ana", 1625.1);
        bank.addAccount("Alex", 444.4);
        bank.addAccount("Dan", 905.3);
        bank.addAccount("Ioana", 297.6);
        bank.addAccount("Elena", 555.7);
        bank.printAccounts();
        System.out.println();
        bank.printAccounts(300, 700);
    }
}
