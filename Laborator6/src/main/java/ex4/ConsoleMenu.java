package ex4;

import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class ConsoleMenu {

    Dictionary dictionary = new Dictionary();
    private final Scanner input = new Scanner(System.in);
    Scanner scanner = new Scanner(System.in);

    private void exit() {
        System.out.println("Exiting...");
        System.exit(1);
    }

    public void display() {
        System.out.println("-- Menu --");
        System.out.println(
                "Select an option: \n" +
                        "  1) Add word\n" +
                        "  2) Get definition\n" +
                        "  3) Get all words \n" +
                        "  4) Get all definitions \n" +
                        "  5) Exit\n "
        );

        int selection = input.nextInt();
        input.nextLine();
        String word, definition;

        switch (selection) {
            case 1:
                word = scanner.nextLine();
                definition = scanner.nextLine();
                dictionary.addWord(new Word(word), new Definition(definition));
                break;
            case 2:
                word = scanner.nextLine();
                Word wrd = new Word(word);
                Definition def;
                def = dictionary.getDefinition(wrd);
                System.out.println(wrd + ":" + def);
                break;
            case 3:
                Set<Word> words;
                words = dictionary.getAllWords();
                System.out.println(words);
                break;
            case 4:
                List<Definition> definitions;
                definitions = dictionary.getAllDefinitions();
                System.out.println(definitions);
                break;
            case 5:
                this.exit();
                break;
            default:
                System.out.println("Invalid selection.");
                break;
        }


    }
}