package ex1;

public class Main {
    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Ana", 772.3);
        b1.withdraw(649);
        System.out.println(b1.getBalance());
        BankAccount b2 = new BankAccount("Ana", 772.3);
        if (b1.equals(b2))
            System.out.println(b1 + " and " + b2 + " are equals");
        else
            System.out.println(b1 + " and " + b2 + " are NOT equals");
        BankAccount b3 = new BankAccount("Ana", 772.3);
        if (b2.equals(b3))
            System.out.println(b2 + " and " + b3 + " are equals");
        else
            System.out.println(b2 + " and " + b3 + " are NOT equals");
        BankAccount b4 = new BankAccount("Dan", 342.5);
        if (b3.equals(b4))
            System.out.println(b3 + " and " + b4 + " are equals");
        else
            System.out.println(b3 + " and " + b4 + " are NOT equals");
    }
}
