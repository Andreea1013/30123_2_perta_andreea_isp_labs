package ex2;

import ex1.BankAccount;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Bank {

    private List<BankAccount> bankAccounts = new ArrayList<>();

    public void addAccount(String owner, double balance) {
        this.bankAccounts.add(new BankAccount(owner, balance));
    }

    public void printAccounts() {
        this.bankAccounts.stream()
                .sorted(Comparator.comparingDouble(BankAccount::getBalance))
                .forEach(System.out::println);
    }

    public void printAccounts(double minBalance, double maxBalance) {
        this.bankAccounts.stream()
                .filter(bankAccount -> bankAccount.getBalance() >= minBalance && bankAccount.getBalance() <= maxBalance)
                .sorted(Comparator.comparingDouble(BankAccount::getBalance))
                .forEach(System.out::println);
    }

    public BankAccount getAccount(String owner) {
        return bankAccounts.stream()
                .filter(bankAccount -> bankAccount.getOwner().equals(owner))
                .findFirst()
                .orElse(null);
    }


}
