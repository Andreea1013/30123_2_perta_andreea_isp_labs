package LibraryManagement;

import java.util.Objects;

public class Book {
    String title;
    String author;
    String ISBN;
    int numberOfPages;
    int numberOfCopies;
    int shelfNumber;

    public Book() {
    }

    public Book(String title, String author, String ISBN, int numberOfPages, int numberOfCopies, int shelfNumber) {
        this.title = title;
        this.author = author;
        this.ISBN = ISBN;
        this.numberOfPages = numberOfPages;
        this.numberOfCopies = numberOfCopies;
        this.shelfNumber = shelfNumber;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public int getNumberOfCopies() {
        return numberOfCopies;
    }

    public void setNumberOfCopies(int numberOfCopies) {
        this.numberOfCopies = numberOfCopies;
    }

    public int getShelfNumber() {
        return shelfNumber;
    }

    public void setShelfNumber(int shelfNumber) {
        this.shelfNumber = shelfNumber;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Book book = (Book) o;
        return numberOfPages == book.numberOfPages && numberOfCopies == book.numberOfCopies && shelfNumber == book.shelfNumber && Objects.equals(title, book.title) && Objects.equals(author, book.author) && Objects.equals(ISBN, book.ISBN);
    }

    @Override
    public int hashCode() {
        return Objects.hash(title, author, ISBN, numberOfPages, numberOfCopies, shelfNumber);
    }

    @Override
    public String toString() {
        return "Title: '" + title + '\'' +
                ", Author: " + author +
                ", ISBN: " + ISBN +
                ", Number of pages: " + numberOfPages +
                ", Number of copies: " + numberOfCopies +
                ", Shelf number: " + shelfNumber;
    }
}
