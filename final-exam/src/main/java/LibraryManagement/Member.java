package LibraryManagement;

import java.util.Objects;

public class Member {
    int memberID;
    String name;
    String email;
    String password;
    int numberOfBorrowedBooks;

    public Member() {
    }

    public Member(int memberID, String name, String email, String password, int numberOfBorrowedBooks) {
        this.memberID = memberID;
        this.name = name;
        this.email = email;
        this.password = password;
        this.numberOfBorrowedBooks = numberOfBorrowedBooks;
    }

    public int getMemberID() {
        return memberID;
    }

    public void setMemberID(int memberID) {
        this.memberID = memberID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public int getNumberOfBorrowedBooks() {
        return numberOfBorrowedBooks;
    }

    public void setNumberOfBorrowedBooks(int numberOfBorrowedBooks) {
        this.numberOfBorrowedBooks = numberOfBorrowedBooks;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Member member = (Member) o;
        return memberID == member.memberID && numberOfBorrowedBooks == member.numberOfBorrowedBooks && Objects.equals(name, member.name) && Objects.equals(email, member.email) && Objects.equals(password, member.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(memberID, name, email, password, numberOfBorrowedBooks);
    }

    @Override
    public String toString() {
        return "Member ID: " + memberID +
                ", Name: " + name +
                ", Email: " + email +
                ", Password: " + password +
                ", Number of borrowed books: " + numberOfBorrowedBooks;
    }
}
