package LibraryManagement;

import javax.swing.*;

public class MembersList {
    private JList list2;
    private JPanel membersListPanel;
    private JLabel imageMemberLabel;
    ImageIcon image = new ImageIcon("members.jpg");

    public JLabel getImageMemberLabel() {
        return imageMemberLabel;
    }

    public JList getList2() {
        return list2;
    }

    public JPanel getMembersListPanel() {
        return membersListPanel;
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("MembersList");
        frame.setContentPane(new MembersList().membersListPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
