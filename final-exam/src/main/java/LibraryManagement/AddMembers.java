package LibraryManagement;

import javax.swing.*;

public class AddMembers {
    private JPanel membersPanel;
    private JTextField IDField;
    private JTextField nameField;
    private JTextField emailField;
    private JTextField passwordField;
    private JButton insertTheInformationMembersButton;
    private JTextField numberOfBorrowedBooksField;
    private JButton listOfMembersButton;

    public JButton getListOfMembersButton() {
        return listOfMembersButton;
    }

    public JPanel getMembersPanel() {
        return membersPanel;
    }

    public JTextField getIDField() {
        return IDField;
    }

    public JTextField getNameField() {
        return nameField;
    }

    public JTextField getEmailField() {
        return emailField;
    }

    public JTextField getPasswordField() {
        return passwordField;
    }

    public JTextField getNumberOfBorrowedBooksField() {
        return numberOfBorrowedBooksField;
    }

    public JButton getInsertTheInformationMembersButton() {
        return insertTheInformationMembersButton;
    }

    public static void main(String[] args) {
        JFrame membersFrame = new JFrame("AddMembers");
        membersFrame.setContentPane(new AddMembers().membersPanel);
        membersFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        membersFrame.pack();
        membersFrame.setVisible(true);
    }
}
