package LibraryManagement;

import javax.swing.*;

public class AddBooks {
    private JPanel booksPanel;
    private JTextField bookTitleField;
    private JTextField authorField;
    private JTextField isbnField;
    private JTextField numberOfPagesField;
    private JTextField numberOfCopiesField;
    private JTextField shelfNumberField;
    private JButton insertTheInformationBookButton;
    private JButton listOfBooksButton;

    public JButton getListOfBooksButton() {
        return listOfBooksButton;
    }

    public JButton getInsertTheInformationBookButton() {
        return insertTheInformationBookButton;
    }

    public JTextField getBookTitleField() {
        return bookTitleField;
    }

    public JTextField getAuthorField() {
        return authorField;
    }

    public JTextField getIsbnField() {
        return isbnField;
    }

    public JTextField getNumberOfPagesField() {
        return numberOfPagesField;
    }

    public JTextField getNumberOfCopiesField() {
        return numberOfCopiesField;
    }

    public JTextField getShelfNumberField() {
        return shelfNumberField;
    }

    public JPanel getBooksPanel() {
        return booksPanel;
    }

    public static void main(String[] args) {
        JFrame booksFrame = new JFrame("AddBooks");
        booksFrame.setContentPane(new AddBooks().booksPanel);
        booksFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        booksFrame.pack();
        booksFrame.setVisible(true);
    }
}
