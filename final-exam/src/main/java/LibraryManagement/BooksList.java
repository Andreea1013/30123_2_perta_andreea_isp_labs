package LibraryManagement;

import javax.swing.*;

public class BooksList {
    private JPanel booksListPanel;
    private JList list1;
    ImageIcon icon = new ImageIcon("books.jpg");
    private JLabel imageBookLabel;

    public JLabel getImageBookLabel() {
        return imageBookLabel;
    }

    public JPanel getBooksListPanel() {
        return booksListPanel;
    }

    public JList getList1() {
        return list1;
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("BooksList");

        frame.setContentPane(new BooksList().booksListPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
