package LibraryManagement;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.Vector;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class Controller {
    private static final Vector<Book> books = new Vector<>(0);
    private static final Vector<Member> members = new Vector<>(0);

    public static void main(String[] args) {
        JFrame booksFrame = new JFrame("AddBooks");
        var addBooks = new AddBooks();

        addBooks.getInsertTheInformationBookButton().addActionListener(e -> {
            String title = addBooks.getBookTitleField().getText();
            String author = addBooks.getAuthorField().getText();
            String ISBN = addBooks.getIsbnField().getText();
            Integer numberOfPages = Integer.parseInt(addBooks.getNumberOfPagesField().getText());
            Integer numberOfCopies = Integer.parseInt(addBooks.getNumberOfCopiesField().getText());
            Integer shelfNumber = Integer.parseInt(addBooks.getShelfNumberField().getText());

            books.add(new Book(title, author, ISBN, numberOfPages, numberOfCopies, shelfNumber));
            File booksFile = new File("src/main/resources/List of Books");
            FileOutputStream f1 = null;
            try {
                f1 = new FileOutputStream(booksFile);
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }

            BufferedWriter booksList = new BufferedWriter(new OutputStreamWriter(f1));
            for (int i = 0; i < books.size(); i++) {
                try {
                    booksList.write(String.valueOf(books.get(i)));
                    booksList.newLine();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
            try {
                booksList.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        var listOfBooks = new BooksList();
        addBooks.getListOfBooksButton().addActionListener(e -> {
            JFrame frame = new JFrame("ListOfBooks");
            booksFrame.dispose();
            listOfBooks.getImageBookLabel().setIcon(listOfBooks.icon);
            frame.setContentPane(listOfBooks.getBooksListPanel());
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setPreferredSize(new Dimension(1100, 320));
            frame.pack();
            frame.setVisible(true);
            listOfBooks.getList1().setListData(books);
        });

        booksFrame.setContentPane(addBooks.getBooksPanel());
        booksFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        booksFrame.pack();
        booksFrame.setVisible(true);

        JFrame membersFrame = new JFrame("AddMembers");
        var addMembers = new AddMembers();
        addMembers.getInsertTheInformationMembersButton().addActionListener(e -> {
            Integer memberID = Integer.parseInt(addMembers.getIDField().getText());
            String name = addMembers.getNameField().getText();
            String email = addMembers.getEmailField().getText();
            String password = addMembers.getPasswordField().getText();
            Integer numberOfBorrowedBooks = Integer.parseInt(addMembers.getNumberOfBorrowedBooksField().getText());

            members.add(new Member(memberID, name, email, password, numberOfBorrowedBooks));

            File membersFile = new File("src/main/resources/List of Members");
            FileOutputStream f2 = null;
            try {
                f2 = new FileOutputStream(membersFile);
            } catch (FileNotFoundException fileNotFoundException) {
                fileNotFoundException.printStackTrace();
            }

            BufferedWriter membersList = new BufferedWriter(new OutputStreamWriter(f2));
            for (int i = 0; i < members.size(); i++) {
                try {
                    membersList.write(String.valueOf(members.get(i)));
                    membersList.newLine();
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                }
            }
            try {
                membersList.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        });

        var listOfMembers = new MembersList();
        addMembers.getListOfMembersButton().addActionListener(e -> {
            JFrame frame = new JFrame("ListOfMembers");
            membersFrame.dispose();
            listOfMembers.getImageMemberLabel().setIcon(listOfMembers.image);
            frame.setContentPane(listOfMembers.getMembersListPanel());
            frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
            frame.setPreferredSize(new Dimension(1100, 300));
            frame.pack();
            frame.setVisible(true);
            listOfMembers.getList2().setListData(members);
        });

        membersFrame.setContentPane(addMembers.getMembersPanel());
        membersFrame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        membersFrame.pack();
        membersFrame.setVisible(true);
    }
}
