package ex1;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestShape {
    @Test
    void TestgetArea() {
        Shape[] shape = new Shape[3];
        shape[0] = new Circle();
        shape[1] = new Rectangle(2, 3);
        shape[2] = new Square(5);
        assertEquals(3.141592653589793, shape[0].getArea());
        assertEquals(6, shape[1].getArea());
        assertEquals(25, shape[2].getArea());
    }

    @Test
    void TestgetPerimeter() {
        Shape[] shape = new Shape[3];
        shape[0] = new Circle();
        shape[1] = new Rectangle(2, 3);
        shape[2] = new Square(5);
        assertEquals(6.283185307179586, shape[0].getPerimeter());
        assertEquals(10, shape[1].getPerimeter());
        assertEquals(20, shape[2].getPerimeter());
    }

}
