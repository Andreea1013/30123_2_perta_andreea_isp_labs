package ex4;

import ex3.LightSensor;
import ex3.TemperatureSensor;

public class Controller {
    private static Controller control;
    TemperatureSensor tempSensor = new TemperatureSensor();
    LightSensor lightSensor = new LightSensor();

    private Controller() {
    }

    public static Controller getController() {
        if (control == null) {
            control = new Controller();
        }
        return control;
    }

    void Control() throws InterruptedException {
        for (int i = 0; i < 20; i++) {
            System.out.println(tempSensor.readValue());
            System.out.println(lightSensor.readValue());
            Thread.sleep(1000);
        }
    }
}
