package ex3;

public class Controller {
    TemperatureSensor tempSensor = new TemperatureSensor();
    LightSensor lightSensor = new LightSensor();

    void Control() throws InterruptedException {
        for (int i = 0; i < 20; i++) {
            System.out.println(tempSensor.readValue());
            System.out.println(lightSensor.readValue());
            Thread.sleep(1000);
        }
    }
}
