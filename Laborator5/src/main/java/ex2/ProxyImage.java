package ex2;

public class ProxyImage implements Image {

    private Image image;
    private String fileName;
    private boolean type;

    public ProxyImage(String fileName, boolean type) {
        this.fileName = fileName;
        this.type = type;
    }

    @Override
    public void display() {
        if (type) {
            if (image == null) {
                image = new RealImage(fileName);
            }
        } else {
            if (image == null) {
                image = new RotatedImage(fileName);
            }
        }
        image.display();

    }

}
