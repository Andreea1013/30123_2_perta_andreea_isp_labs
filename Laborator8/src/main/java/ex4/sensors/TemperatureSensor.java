package ex4.sensors;

public class TemperatureSensor extends Sensor {
    private int value;

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}
