package ex4;

import ex4.events.Event;
import ex4.events.FireEvent;
import ex4.events.TemperatureEvent;
import ex4.sensors.FireSensor;
import ex4.sensors.Sensor;
import ex4.sensors.TemperatureSensor;
import ex4.units.*;

import java.util.ArrayList;
import java.util.List;


import org.apache.log4j.Logger;

public class Controller extends Home implements Unit {

    private static final Logger LOGGER = Logger.getLogger(Controller.class);
    private final Unit gsmUnit = new GSMUnit();
    private final Unit alarmUnit = new AlarmUnit();
    private final Unit coolingUnit = new CoolingUnit();
    private final Unit heatingUnit = new HeatingUnit();
    private static final int PRESET_TEMPERATURE_VALUE = 25;
    Sensor temperatureSensor = new TemperatureSensor();
    List<Sensor> fireSensor = new ArrayList<>(5);
    private static Controller instance;

    private Controller() {
        for (int i = 0; i < 5; i++) {
            fireSensor.add(new FireSensor());
        }
    }

    public static Controller getInstance() {
        if (instance == null) {
            instance = new Controller();
        }
        return instance;
    }

    @Override
    protected void setValueInEnvironment(Event event) {
        switch (event.getType()) {
            case TEMPERATURE:
                this.temperatureSensor.setValue(((TemperatureEvent) event).getValue());
                break;
            case FIRE:
                this.fireSensor.get(r.nextInt(fireSensor.size())).setValue(((FireEvent) event).isSmoke() ? 1 : 0);
                break;
            case NONE:
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + event.getType());
        }

    }

    @Override
    protected void controlStep() {
        LOGGER.info("Control step:");
        fireSensor.stream().filter(sensor -> sensor.getValue() == 1).findAny()
                .ifPresent(sensor -> {
                    alarmUnit.execute();
                    gsmUnit.execute();
                });
        if (temperatureSensor.getValue() < PRESET_TEMPERATURE_VALUE) {
            heatingUnit.execute();
        } else {
            coolingUnit.execute();
        }
        LOGGER.info("");
        fireSensor.forEach(sensor -> sensor.setValue(0));
    }

    @Override
    public void execute() {
        super.simulate();
    }


}
