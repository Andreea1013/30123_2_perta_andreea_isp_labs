package ex1;

public class Main {
    public static void main(String args[]) {
        TemperatureSensor t = new TemperatureSensor();
        Interface in = new Interface();
        t.register(in);
        new Thread(t).start();
    }
}
