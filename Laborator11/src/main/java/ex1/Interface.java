package ex1;

import javax.swing.*;
import java.awt.*;

public class Interface extends JFrame implements Observer {
    JTextField textField;

    public Interface() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setSize(300, 300);
        this.setLayout(null);
        setVisible(true);
        textField = new JTextField();
        textField.setBounds(50, 50, 50, 20);
        add(textField);
    }

    @Override
    public void update(Object event) {
        textField.setText(event.toString());
    }
}
