package ex2;

import javax.swing.*;
import java.util.Vector;

import static javax.swing.WindowConstants.EXIT_ON_CLOSE;

public class Controller {

    private static final Vector<Product> products = new Vector<>(0);

    public static void main(String[] args) {
        JFrame frame = new JFrame("ProductInterface");
        var productInterface = new ProductInterface();
        productInterface.getAddNewProductButton().addActionListener(e -> {
            String productName = productInterface.getNameField().getText();
            Integer productQuantity = Integer.parseInt(productInterface.getQuantityField().getText());
            Double productPrice = Double.parseDouble(productInterface.getPriceField().getText());

            products.add(new Product(productName, productQuantity, productPrice));
        });

        productInterface.getViewAvailableProductsButton().addActionListener(e -> productInterface.getProductsList().setListData(products));
        productInterface.getDeleteProductButton().addActionListener(e -> products.remove(productInterface.getProductsList().getSelectedValue()));
        productInterface.getUpdateQuantityButton().addActionListener(e -> products.get(productInterface.getProductsList().getSelectedIndex()).setProductQuantity(Integer.parseInt(productInterface.getNewQuantityField().getText())));

        frame.setContentPane(productInterface.getPanel());
        frame.setDefaultCloseOperation(EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
