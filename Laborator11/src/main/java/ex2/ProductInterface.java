package ex2;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ProductInterface {
    public JPanel panel;
    private JButton addNewProductButton;
    private JButton viewAvailableProductsButton;
    private JButton deleteProductButton;
    private JButton updateQuantityButton;
    private JList productsList;
    private JTextField NameField;
    private JTextField QuantityField;
    private JTextField PriceField;
    private JTextField NewQuantityField;

    public JButton getAddNewProductButton() {
        return addNewProductButton;
    }

    public JButton getViewAvailableProductsButton() {
        return viewAvailableProductsButton;
    }

    public JButton getDeleteProductButton() {
        return deleteProductButton;
    }

    public JButton getUpdateQuantityButton() {
        return updateQuantityButton;
    }

    public JTextField getNameField() {
        return NameField;
    }

    public JTextField getQuantityField() {
        return QuantityField;
    }

    public JTextField getPriceField() {
        return PriceField;
    }

    public JList getProductsList() {
        return productsList;
    }

    public JPanel getPanel() {
        return panel;
    }

    public JTextField getNewQuantityField() {
        return NewQuantityField;
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("ProductInterface");
        frame.setContentPane(new ProductInterface().panel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
