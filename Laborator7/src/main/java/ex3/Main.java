package ex3;

public class Main {
    public static void main(String[] args) {
        EncryptFileService.encryptAndSaveContent("src/main/resources/fileToBeEncrypted.txt");
        EncryptFileService.decryptAndSaveContent("src/main/resources/fileToBeDecrypted.txt");
    }
}
