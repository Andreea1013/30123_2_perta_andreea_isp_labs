package ex2;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Main {
    public static void main(String[] args) throws FileNotFoundException {
        int count = 0;
        try (BufferedReader in = new BufferedReader(new FileReader("src/main/resources/data.txt"))) {
            String s;
            while ((s = in.readLine()) != null) {
                for (String letter : s.split("")) {
                    if (letter.equals(args[0])) {
                        count++;
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println("Letter: " + args[0] + " appears " + count + " times in data.txt file");
    }
}
