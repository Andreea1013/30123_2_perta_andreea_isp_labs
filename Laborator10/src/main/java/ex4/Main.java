package ex4;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Robot> robotList = new ArrayList<>(10);
        initializeRobots(robotList);
        SetPosition setPosition = new SetPosition(robotList);
        VerifyCollide verifyCollide = new VerifyCollide(robotList);
        setPosition.start();
        verifyCollide.start();
    }

    private static void initializeRobots(List<Robot> robotList) {
        for (int i = 0; i < 10; i++) {
            robotList.add(new Robot(i, i * 2));
        }
    }
}
