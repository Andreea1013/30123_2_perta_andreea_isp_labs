package ex3;

public class Thread2 extends Thread {
    private final Thread1 thread1;

    public Thread2(Thread1 thread1) {
        this.thread1 = thread1;
    }

    @Override
    public void run() {
        try {
            thread1.join();
        } catch (InterruptedException interruptedException) {
            interruptedException.printStackTrace();
        }
        int i = 100;
        while (++i < 201) {
            System.out.println(this.getName() + " i=" + i);
        }
    }
}
