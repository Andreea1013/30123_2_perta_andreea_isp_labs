package ex6;

import javax.swing.*;
import java.awt.*;

public class Interface extends JFrame {
    JTextField text = new JTextField();
    volatile boolean started = false;
    volatile int nr;

    public Interface() {
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setSize(300, 60);
        this.setLayout(new GridLayout(1, 3));
        JButton start = new JButton("Start/Stop");
        JButton reset = new JButton("reset");
        start.addActionListener(e -> {
            synchronized (Interface.this) {
                notify();
                Interface.this.started = !Interface.this.started;
            }
        });
        reset.addActionListener(e -> {
            Interface.this.nr = 0;
            text.setText(nr + "");
        });
        this.add(text);
        this.add(reset);
        this.add(start);
        this.setVisible(true);
    }

}
