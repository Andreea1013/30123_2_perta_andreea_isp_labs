package ex6;

public class Chronometer extends Thread {
    Interface i1;

    public Chronometer(Interface i1) {
        this.i1 = i1;
    }

    @Override
    public void run() {
        while (true) {
            if (i1.started) {
                i1.nr++;
                i1.text.setText(i1.nr + "");
            } else {
                synchronized (i1) {
                    try {
                        i1.wait();
                    } catch (InterruptedException interruptedException) {
                        interruptedException.printStackTrace();
                    }
                }
            }
            try {
                Thread.sleep(100);
            } catch (InterruptedException interruptedException) {
                interruptedException.printStackTrace();
            }
        }
    }


}